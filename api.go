package viewmaster

import (
	"context"
)

type Sequenced interface {
	//comparable ... why would we need our values to be comparable??
	Sequence() uint64
}

type Follower[Key comparable, Value Sequenced] interface {
	// read a batch of updates from the follower.  The context
	// will be canceled in order to make additional
	// Follow/Unfollow calls.  As a result, Follower need not be
	// concurrency safe; the engine will not call Follow or
	// Unfollow until after Read has returned.  But that also
	// means that Read *must* respect context cancelation.
	Read(context.Context) (map[Key]Value, error)

	// Follow tells the follower that we want to follow the given
	// key.  Any subsequent change of value for the key should be
	// returned from a Read() call.
	Follow(Key)

	// Unfollow tells the follower that we are no longer interested
	// in the given key
	Unfollow(Key)
}
