module bitbucket.org/fufoo/viewmaster

go 1.18

require (
	bitbucket.org/dkolbly/logging v0.9.5 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	golang.org/x/sys v0.0.0-20190124100055-b90733256f2e // indirect
)
