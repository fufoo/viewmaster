# View Master

This package implements a dependency engine.

**NOTE** This package uses Go 1.18 parameterized types (indeed, it is
my first real application of generic types in Go; semi-successful
IMHO); the engine is defined over four types that represent the keys
and values of the client protocol and the underlying dependency store.

```
                          +-----------------------+
                          |                       |
                          |                       |
                       (Watch)                    |        +-------------+
                          |                       |        |    Target   |
       Application  -->   |         Engine        ------>(Eval)          |
                          |                       |        |             |--+
                   (*Stream.Read)                 |        +-------------+  |
                          |                       |           |             |
                          |                       |           +-------------+
                          +----------|------------+
                                     |
                                     v

                   +--(Follow)---(Unfollow)---(Read)--+
                   |                                  |
                   |           Follower               |
                   |                                  |
                   +----------------------------------+
```

Note that the type parameterization allows us to build a *highly abstract*
dependency resolution engine.  The particular parameterization
was chosen to cover several use cases known to me:

- A home automation system; different devices may mirror their state to 
  scripted calculations across various devices.  A dashboard might depend
  on subviews across lots of different devices.
- A build system; build processes can watch for changes to their
  dependencies and rebuild when a dependency changes.
- Web clients that want to observe (filtered) views on underlying data
  and to be notified when views change.  Furthermore, a given view can
  depend on various observable ground truths.  For example, a view
  might represent information about the most recent 10 jobs in a job
  queueing system.  The jobs themselves can change, and the list of
  jobs can change.

## Type Parameters

These are the type parameters

- **Key**: A key is the name of an observable value.  When using Fufoo (foo)
  as the underlying store, a key could be a *domain* plus a *name*.
- **Value**: A value is the (current) value of an observable.  Values
   must be *Sequenced*, meaning that they can produce a uint64 which,
   when can compared with the sequence from a different Value as a
   model of causal relationships.  That is, higher sequences "come
   later".  This is to deal with race conditions between the follower
   and the evaluation, since a later value for an observable might
   arrive at the engine by two paths and the sequence is used to
   disambiguate them.  When using foo, a value could be an observable
   sequence with its pointer value.
- **View**: A view is something that targets know how to evaluate
- **Snapshot**: A snapshot is what the evaluation of a view produces

## Interface Types

- **Target**: A target knows how to evaluate a view to produce a snapshot,
  and reporting on what the value of the view depends upon
- **Follower**: A follower knows how to report on changes to observable keys.

## Example

See `example/spreadsheet/` for an example and test case, built to
model a spreadsheet using this dependency engine.


## Linked List

This module also includes a generic doubly linked list, which is used
in a couple of places in the engine but exposed as a separate package
in case there is use in reusing it.
