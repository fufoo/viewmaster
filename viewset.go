package viewmaster

import (
	"math/bits"
)

type viewIndexSet uint64

func (v viewIndexSet) IsEmpty() bool {
	return v == 0
}

func (v *viewIndexSet) Add(k int) {
	*v |= 1 << k
}

func (v viewIndexSet) Len() int {
	return bits.OnesCount64(uint64(v))
}

func (v viewIndexSet) ForEach(fn func(int)) {
	x := uint64(v)
	for x != 0 {
		i := bits.TrailingZeros64(x)
		fn(i)
		x &^= uint64(1) << i
	}
}

func (v *viewIndexSet) Clear() {
	*v = 0
}
