package viewmaster

type target[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	xtarget Target[Key, Value, View, Snapshot]
	views   map[View]*view[Key, Value, View, Snapshot]
	dirty   []*view[Key, Value, View, Snapshot]
}

type view[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	target  *target[Key, Value, View, Snapshot]
	xview   View                                             // the application View we represent
	streams map[*Stream[Key, Value, View, Snapshot]]struct{} // which streams are watching this?
	isDirty bool                                             // are we queued on the target?
	hasSnap bool                                             // do we have a snaphot value at all?
	snap    Snapshot
	deps    []*obs[Key, Value, View, Snapshot]
}

func (t *target[Key, Value, View, Snapshot]) removeDirtyView(v *view[Key, Value, View, Snapshot]) {
	for i, dirty := range t.dirty {
		if dirty == v {
			n := len(t.dirty)
			t.dirty[i] = t.dirty[n-1]
			t.dirty = t.dirty[:n-1]
			return
		}
	}
	// this should not happen; implies we were called for the
	// wrong reason, or that view.isDirty is out of sync with
	// target.dirty[]
	panic("could not remove dirty view, it wasn't found")
}

func (v *view[Key, Value, View, Snapshot]) shutdown(engine *Engine[Key, Value, View, Snapshot]) {
	v.tracef("shutting down")
	if len(v.streams) != 0 {
		panic("there are still streams") // should not happen
	}
	// detach us from all the obs
	for _, d := range v.deps {
		delete(d.views, v)
		if len(d.views) == 0 {
			v.tracef("no more views on observable %#v", d.key)
			// don't do anything yet... TODO clean up obs after a while
		}
	}
	if v.isDirty {
		// remove us from the dirty list
		v.tracef("removing from dirty list")
		v.target.removeDirtyView(v)
	}

	// remove us from the target's view index
	delete(v.target.views, v.xview)
}

func (engine *Engine[Key, Value, View, Snapshot]) bindTarget(t Target[Key, Value, View, Snapshot]) *target[Key, Value, View, Snapshot] {
	i, ok := engine.targets[t]
	if !ok {
		i = &target[Key, Value, View, Snapshot]{
			xtarget: t,
			views:   make(map[View]*view[Key, Value, View, Snapshot]),
		}
		engine.targets[t] = i
	}
	return i
}

func (t *target[Key, Value, View, Snapshot]) bindView(xview View) *view[Key, Value, View, Snapshot] {
	v, ok := t.views[xview]
	if !ok {
		v = &view[Key, Value, View, Snapshot]{
			target:  t,
			xview:   xview,
			streams: make(map[*Stream[Key, Value, View, Snapshot]]struct{}),
		}
		t.views[xview] = v
	}
	return v
}
