package viewmaster

import (
	"io"
	"sync"

	"bitbucket.org/fufoo/viewmaster/list"
)

type Engine[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	streamid        uint64
	calls           chan<- syscall[Key, Value, View, Snapshot]
	targets         map[Target[Key, Value, View, Snapshot]]*target[Key, Value, View, Snapshot]
	observables     map[Key]*obs[Key, Value, View, Snapshot]
	targetWorkQueue list.List[*target[Key, Value, View, Snapshot]]
	waitingWorkers  list.List[*assignWorkCall[Key, Value, View, Snapshot]]
	workers         []*worker[Key, Value, View, Snapshot]
	workerWait      sync.WaitGroup
	followerWait    sync.WaitGroup
	followChange    chan<- [2][]*obs[Key, Value, View, Snapshot]
}

type syscall[Key comparable, Value Sequenced, View comparable, Snapshot any] interface {
	apply(*Engine[Key, Value, View, Snapshot])
}

// Key defines the domain of things that are observable, Value defines
// the range of those observations.
//
// Target will define the type of thing that is queried.  View defines the
// domain of computations that can be queried from targets, and
// Snapshot defines the range of possible evaluations for those views.
//
// Note that values must implement Sequenced in order to detect
// certain kinds of races.  In particular, if a provider returns a
// snapshot computed using a certain set of Values but other values
// are known in the state of the engine, we need to tell if the values
// observed by the provider are from before or after the values known
// to the engine.  An alternative approach would be to require the
// provider to access data by means of the engine's store, but that
// appraoch does not map very well to some of the intended use cases
// (in particular, where the provider is actually a remote process and
// the value store is an external database available to both parties)
func New[Key comparable, Value Sequenced, View comparable, Snapshot any](f Follower[Key, Value], numWorkers int) *Engine[Key, Value, View, Snapshot] {

	ch := make(chan syscall[Key, Value, View, Snapshot], 100)
	fch := make(chan [2][]*obs[Key, Value, View, Snapshot])

	e := &Engine[Key, Value, View, Snapshot]{
		calls:        ch,
		targets:      make(map[Target[Key, Value, View, Snapshot]]*target[Key, Value, View, Snapshot]),
		observables:  make(map[Key]*obs[Key, Value, View, Snapshot]),
		followChange: fch,
	}

	e.workerWait.Add(numWorkers)

	for i := 0; i < numWorkers; i++ {
		w := &worker[Key, Value, View, Snapshot]{
			id:     i + 1,
			engine: e,
		}
		go w.run()
		e.workers = append(e.workers, w)
	}

	e.followerWait.Add(1)
	go e.keepFollowing(fch, f)

	go e.run(ch)
	return e
}

func (e *Engine[Key, Value, View, Snapshot]) Shutdown() {

	// shut down all the *waiting* workers
	for !e.waitingWorkers.IsEmpty() {
		w := e.waitingWorkers.PopFront()
		w.ret <- io.EOF
	}

	// wait for all the workers to finish (note that while we are
	// waiting, some workers may still be finishing up their work,
	// even sending results back to the engine)
	e.workerWait.Wait()

	// shut down the following channel
	close(e.followChange)
	// wait for the follower to finish
	e.followerWait.Wait()

	// shut down the command channel
	close(e.calls)
}
