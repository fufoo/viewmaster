package list

type List[T any] struct {
	free *Node[T]
	head *Node[T]
}

type Node[T any] struct {
	pred *Node[T]
	succ *Node[T]
	data T
}

func (l *List[T]) IsEmpty() bool {
	return l.head == nil
}

func (l *List[T]) PopFront() T {
	h := l.head
	if h.succ == h {
		l.head = nil
	} else {
		h.succ.pred, h.pred.succ = h.pred, h.succ
		l.head = h.succ
	}

	// put the old head on the free list
	h.succ = l.free
	l.free = h

	// zero out the data, in case it needs GCing
	x := h.data
	var zero T
	h.data = zero
	return x
}

func (l *List[T]) PushBack(x T) {
	t := l.free
	if t == nil {
		t = new(Node[T])
	} else {
		l.free = t.succ
	}
	t.data = x

	h := l.head
	if h == nil {
		t.pred, t.succ = t, t
		l.head = t
	} else {
		t.pred, t.succ = h.pred, h
		t.pred.succ = t
		h.pred = t
	}
}
