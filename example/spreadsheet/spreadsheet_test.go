package spreadsheet

import (
	"context"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"

	"bitbucket.org/dkolbly/logging"
	"bitbucket.org/fufoo/viewmaster"
)

var log = logging.New("spreadsheet")

type Cell string // a cell within a document

type GCR struct { // global cell reference
	document string
	cell     Cell
}

type Expr struct {
	user string
	text string
}

type CellValue struct {
	seq     int
	err     string
	numeric int
}

type Snapshot struct {
	err     string
	numeric int
}

func (c *CellValue) Sequence() uint64 {
	return uint64(c.seq)
}

type Computer = viewmaster.Engine[GCR, *CellValue, Expr, Snapshot]

type DocumentStore struct {
	lock      sync.Mutex
	following map[GCR]int
	current   map[GCR]*CellValue
	dirty     map[GCR]struct{}
	notify    chan struct{}
}

func (ds *DocumentStore) SetValue(key GCR, value int) {
	ds.lock.Lock()
	defer ds.lock.Unlock()

	ds.dirty[key] = struct{}{}
	cv := &CellValue{
		seq:     100,
		numeric: value,
	}

	if v, ok := ds.current[key]; ok {
		cv.seq = v.seq + 1
	}
	ds.current[key] = cv
	select {
	case ds.notify <- struct{}{}:
	}
}

func (ds *DocumentStore) Read(ctx context.Context) (map[GCR]*CellValue, error) {
	for {
		select {

		case <-ctx.Done():
			return nil, context.Canceled

		case <-ds.notify:
			ix := ds.flushdirty()
			if ix != nil {
				return ix, nil
			}
		}
	}
}

func (ds *DocumentStore) flushdirty() map[GCR]*CellValue {
	ds.lock.Lock()
	defer ds.lock.Unlock()

	// go get the current values as of now
	n := len(ds.dirty)
	if n == 0 {
		return nil
	}

	ix := make(map[GCR]*CellValue, n)

	for k := range ds.dirty {
		ix[k] = ds.current[k]
		ds.following[k] = ds.current[k].seq
		delete(ds.dirty, k)
	}

	return ix
}

func (ds *DocumentStore) Unfollow(k GCR) {
	delete(ds.following, k)
}

func (ds *DocumentStore) Follow(k GCR) {
	cur, ok := ds.current[k]
	if ok {
		ds.following[k] = cur.seq
	} else {
		ds.following[k] = 0
	}
}

// the ExprTarget knows how to evaluate expressions, and is keyed
// to a specific document.  Exprs are scoped to a single document
// (usually), so the ExprTarget's doc essentially configures the
// default document for cell references
type ExprTarget struct {
	doc string
	ds  *DocumentStore
}

func (e ExprTarget) Eval(ctx context.Context, exprs []Expr) ([]Snapshot, []map[GCR]*CellValue, error) {
	log.Debugf(ctx, "========================================")
	n := len(exprs)
	snaps := make([]Snapshot, n)
	deps := make([]map[GCR]*CellValue, n)

	for i, expr := range exprs {
		snaps[i], deps[i] = e.eval(expr.user, expr.text)
		log.Debugf(ctx, "Eval[%d] result is %#v", i, snaps[i])
	}

	return snaps, deps, nil
}

func (e ExprTarget) eval(user, expr string) (Snapshot, map[GCR]*CellValue) {
	log.Debugf(context.TODO(), "Eval in (%s) %q", e.doc, expr)
	if len(expr) == 0 {
		return Snapshot{err: "#MISSING"}, nil
	}
	if expr[0] == '=' {
		return e.evalexpr(user, strings.Split(strings.TrimSpace(expr[1:]), " "))
	}
	var s Snapshot

	n, err := strconv.Atoi(expr)
	if err != nil {
		s.err = err.Error()
	} else {
		s.numeric = n
	}
	return s, nil
}

type evalctx struct {
	user  string
	stack []int
	deps  map[GCR]*CellValue
	ds    *DocumentStore
}

func (ec *evalctx) pop() int {
	n := len(ec.stack)
	top := ec.stack[n-1]
	ec.stack = ec.stack[:n-1]
	return top
}

func (ec *evalctx) push(x int) {
	ec.stack = append(ec.stack, x)
}

var globalCellRef = regexp.MustCompile(`^(.*)!([A-Z]+[0-9]+)$`)
var localCellRef = regexp.MustCompile(`^([A-Z]+[0-9]+)$`)

func (ec *evalctx) setdep(k GCR, val *CellValue) {
	if ec.deps == nil {
		ec.deps = make(map[GCR]*CellValue)
	}
	ec.deps[k] = val
}

type operfn func(*evalctx) error

func plus(ec *evalctx) error {
	// since + commutes, we don't care about order of evaluation here
	ec.push(ec.pop() + ec.pop())
	return nil
}

func times(ec *evalctx) error {
	// since * commutes, we don't care about order of evaluation here
	ec.push(ec.pop() * ec.pop())
	return nil
}

func minus(ec *evalctx) error {
	r := ec.pop()
	l := ec.pop()
	ec.push(l - r)
	return nil
}

func priority(ec *evalctx) error {
	ref := GCR{"UserPriority", "A1"}

	if ec.user == "alice" {
		ref.cell = "A3"
	} else if ec.user == "system" {
		ref.cell = "A2"
	}

	v, ok := ec.ds.current[ref]
	if !ok {
		return fmt.Errorf("#MISSING(%s)", ref.cell)
	}
	ec.setdep(ref, v)

	ec.push(v.numeric)
	return nil
}

var operators = map[string]operfn{
	"+":        plus,
	"-":        minus,
	"*":        times,
	"priority": priority,
}

func (e ExprTarget) evalexpr(user string, words []string) (Snapshot, map[GCR]*CellValue) {

	ec := &evalctx{
		user: user,
		ds:   e.ds,
	}

	for _, word := range words {
		op, ok := operators[word]

		if ok {
			// an operator; note that operators can return
			// errors, and the error can depend on the
			// things (also note that we don't have any
			// nuance; an error from an operator will be
			// reported as being dependent on everything
			// we've seen up until now, even if the
			// *actual* error doesn't depend on any of
			// that)
			err := op(ec)
			if err != nil {
				return Snapshot{err: err.Error()}, ec.deps
			}
		} else if n, err := strconv.Atoi(word); err == nil {
			// a literal value
			ec.stack = append(ec.stack, n)
		} else if m := localCellRef.FindStringSubmatch(word); m != nil {
			// a cell reference
			k := GCR{
				document: e.doc,
				cell:     Cell(m[1]),
			}
			if v, ok := e.ds.current[k]; ok {
				if v.err != "" {
					// report an error, but depend only on this cell
					// since it is a literal reference (if the cell that
					// we look up were a function of something else, then
					// we would need to include those deps too, at least)
					return Snapshot{err: "#ERROR"}, map[GCR]*CellValue{k: v}
				}
				ec.setdep(k, v)
				ec.stack = append(ec.stack, v.numeric)
			} else {
				// note that we report a missing value, but also
				// report the dependency so that if the value is added
				// later, we will reevaluate
				return Snapshot{err: "#MISSING"}, map[GCR]*CellValue{k: nil}
			}
		} else if m := globalCellRef.FindStringSubmatch(word); m != nil {
			// a cell reference to a different document
			//g, n, ok := getcell(m[1], m[2])
			panic("TODO")
		} else {
			// invalid
			return Snapshot{err: "#INVALID"}, nil
		}
	}
	if len(ec.stack) != 1 {
		return Snapshot{err: "#SYNTAX"}, nil
	}
	return Snapshot{numeric: ec.stack[0]}, ec.deps
}

func newdocs() *DocumentStore {
	return &DocumentStore{
		following: make(map[GCR]int),
		current:   make(map[GCR]*CellValue),
		dirty:     make(map[GCR]struct{}),
		notify:    make(chan struct{}, 10),
	}
}

func TestSpreadsheetReadExitsOnShutdown(t *testing.T) {
	docstore := newdocs()

	var engine *Computer = viewmaster.New[GCR, *CellValue, Expr, Snapshot](docstore, 3)

	stream := engine.Watch(ExprTarget{"Page1", docstore}, []Expr{Expr{text: "3"}})

	go func() {
		time.Sleep(20 * time.Millisecond)
		engine.Shutdown()
	}()

	if false {
		_, err := stream.Read(context.Background())
		if err == nil {
			t.Error("expected Read to fail, but didn't")
		} else if err != io.EOF {
			t.Errorf("expected Read to get EOF, but got: %s", err)
		}
	}
}

func TestSpreadsheetContextFreeViews(t *testing.T) {
	docstore := newdocs()

	var engine *Computer = viewmaster.New[GCR, *CellValue, Expr, Snapshot](docstore, 3)
	ctx := context.Background()

	stream := engine.Watch(ExprTarget{"Page1", docstore}, []Expr{Expr{text: "=10 3 - 5 *"}})
	stuff, err := stream.Read(ctx)
	if err != nil {
		t.Fatalf("expected Read to succeed, but failed: %s", err)
	}

	if stuff[0].numeric != 35 {
		t.Fatalf("expected Read to return 35, but returned %d instead", stuff[0].numeric)
	}

	engine.Shutdown()
}

func TestSpreadsheetSimpleReference(t *testing.T) {
	docstore := newdocs()

	docstore.SetValue(GCR{"Page1", "A1"}, 101)

	var engine *Computer = viewmaster.New[GCR, *CellValue, Expr, Snapshot](docstore, 3)
	ctx := context.Background()

	stream := engine.Watch(ExprTarget{"Page1", docstore}, []Expr{Expr{text: "=A1"}})

	stuff, err := stream.Read(ctx)
	if err != nil {
		t.Fatalf("expected Read to succeed, but failed: %s", err)
	}
	if stuff[0].numeric != 101 {
		t.Fatalf("expected Read to return 101, but returned %d instead", stuff[0].numeric)
	}

	// change the value
	t0 := time.Now()
	docstore.SetValue(GCR{"Page1", "A1"}, 303)

	stuff, err = stream.Read(ctx)
	t1 := time.Now()

	if err != nil {
		t.Fatalf("expected Read to succeed, but failed: %s", err)
	}
	if stuff[0].numeric != 303 {
		t.Fatalf("expected second Read to return 303, but returned %d instead", stuff[0].numeric)
	}
	log.Debugf(ctx, "latency %s", t1.Sub(t0))

	engine.Shutdown()
}

func TestSpreadsheetUserContext(t *testing.T) {
	docstore := newdocs()

	docstore.SetValue(GCR{"Page1", "A1"}, 5)
	docstore.SetValue(GCR{"UserPriority", "A1"}, 100)
	docstore.SetValue(GCR{"UserPriority", "A2"}, 50)
	docstore.SetValue(GCR{"UserPriority", "A3"}, 200)

	var engine *Computer = viewmaster.New[GCR, *CellValue, Expr, Snapshot](docstore, 3)
	ctx := context.Background()

	stream := engine.Watch(ExprTarget{"Page1", docstore}, []Expr{
		Expr{user: "alice", text: "=A1 priority +"},
		Expr{user: "bob", text: "=A1 priority +"},
	})

	v, err := stream.Read(ctx)
	if err != nil {
		t.Fatalf("expected Read to succeed, but failed: %s", err)
	}
	if v[0].numeric != 205 {
		t.Fatalf("expected Read to return 205, but returned %d instead", v[0].numeric)
	}
	if v[1].numeric != 105 {
		t.Fatalf("expected Read to return 105, but returned %d instead", v[1].numeric)
	}

	// bump the offset; should drive both views to update
	docstore.SetValue(GCR{"Page1", "A1"}, 7)

	v, err = stream.Read(ctx)
	if err != nil {
		t.Fatalf("expected Read to succeed, but failed: %s", err)
	}
	if v[0].numeric != 207 {
		t.Fatalf("expected Read to return 207, but returned %d instead", v[0].numeric)
	}
	if v[1].numeric != 107 {
		t.Fatalf("expected Read to return 107, but returned %d instead", v[1].numeric)
	}

	engine.Shutdown()
}
