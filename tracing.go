package viewmaster

import (
	"context"
	"fmt"

	"bitbucket.org/dkolbly/logging"
)

var log = logging.New("viewmaster")

var trace = false

func SetTrace(enable bool) {
	trace = enable
}

func (s *Stream[Key, Value, View, Snapshot]) tracef(msg string, args ...interface{}) {
	if trace {
		log.Send(context.TODO(), 1, logging.TRACE,
			fmt.Sprintf(msg, args...),
			logging.F("stream", s.id),
		)
	}
}

func (t *target[Key, Value, View, Snapshot]) tracef(msg string, args ...interface{}) {
	if trace {
		log.Send(context.TODO(), 1, logging.TRACE,
			fmt.Sprintf(msg, args...),
			logging.F("target", fmt.Sprintf("%p", t)),
		)
	}
}

func (v *view[Key, Value, View, Snapshot]) tracef(msg string, args ...interface{}) {
	if trace {
		log.Send(context.TODO(), 1, logging.TRACE,
			fmt.Sprintf(msg, args...),
			logging.F("view", fmt.Sprintf("%p", v)),
		)
	}
}

func (w *worker[Key, Value, View, Snapshot]) tracef(msg string, args ...interface{}) {
	if trace {
		log.Send(context.TODO(), 1, logging.TRACE,
			fmt.Sprintf(msg, args...),
			logging.F("worker", w.id),
		)
	}
}

func (e *Engine[Key, Value, View, Snapshot]) tracef(msg string, args ...interface{}) {
	if trace {
		log.Send(context.TODO(), 1, logging.TRACE,
			fmt.Sprintf(msg, args...),
		)
	}
}
