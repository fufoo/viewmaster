package viewmaster

import (
	"context"
	"errors"
	"io"
	"sync/atomic"
)

var ErrDuplicateRead = errors.New("concurrent call to (*Stream).Read")

type Stream[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	id      uint64
	owner   *Engine[Key, Value, View, Snapshot]
	closed  bool
	ready   viewIndexSet                                // views that have changes to report to Read
	waiter  *readStreamCall[Key, Value, View, Snapshot] // a waiting read stream call, if any
	watches []*view[Key, Value, View, Snapshot]         // map view index to actual view
	watchix map[*view[Key, Value, View, Snapshot]]int   // map view to view index
}

func (s *readStreamCall[Key, Value, View, Snapshot]) apply(engine *Engine[Key, Value, View, Snapshot]) {
	if s.stream.waiter != nil {
		s.ret <- ErrDuplicateRead
		return
	}

	if s.stream.ready.IsEmpty() {
		// nothing is ready yet, so we need to stall
		s.stream.tracef("readStreamCall: nothing is ready; stalling")
		s.stream.waiter = s
		return
	}
	s.stream.tracef("readStreamCall: %d views are ready", s.stream.ready.Len())
	s.resolve(engine)
}

func (s *readStreamCall[Key, Value, View, Snapshot]) resolve(engine *Engine[Key, Value, View, Snapshot]) {
	s.snaps = make(map[int]Snapshot)
	s.stream.ready.ForEach(func(k int) {
		s.snaps[k] = s.stream.watches[k].snap
	})
	s.stream.ready.Clear()
	s.ret <- nil
}

type Target[Key comparable, Value Sequenced, View comparable, Snapshot any] interface {
	Eval(context.Context, []View) ([]Snapshot, []map[Key]Value, error)
}

// Watch creates a stream of updates to a set of views; it returns
// immediately (hence there is no opportunity to cancel) - any
// problems are reported as errors from the Read() call on stream.
//
// Also note there is currently an implementation limit of 64 views
// per watch.  Watch() panics if more views are requested.
func (e *Engine[Key, Value, View, Snapshot]) Watch(target Target[Key, Value, View, Snapshot], views []View) *Stream[Key, Value, View, Snapshot] {

	if len(views) > 64 {
		panic("a maximum of 64 views are allowed per watch")
	}

	s := &Stream[Key, Value, View, Snapshot]{
		owner: e,
		id:    atomic.AddUint64(&e.streamid, 1),
	}

	e.calls <- startStreamCall[Key, Value, View, Snapshot]{
		stream:  s,
		xtarget: target,
		xviews:  views,
	}
	return s
}

type startStreamCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	stream  *Stream[Key, Value, View, Snapshot]
	xviews  []View
	xtarget Target[Key, Value, View, Snapshot]
}

func (call startStreamCall[Key, Value, View, Snapshot]) apply(engine *Engine[Key, Value, View, Snapshot]) {
	stream := call.stream

	stream.tracef("startStreamCall: starting stream")

	// bind to the target
	t := engine.bindTarget(call.xtarget)
	stream.tracef("startStreamCall: bound to target %p", t)

	// bind to the views

	n := len(call.xviews)
	stream.watches = make([]*view[Key, Value, View, Snapshot], n)
	stream.watchix = make(map[*view[Key, Value, View, Snapshot]]int, n)

	for i, xview := range call.xviews {
		v := t.bindView(xview)
		if _, ok := stream.watchix[v]; ok {
			stream.tracef("invalid usage; view is part of this watch already")
			// we never report on this view, it is redundant.  The caller should
			// sort out their business.
			continue
		}
		stream.watches[i] = v

		stream.watchix[v] = i
		v.streams[stream] = struct{}{}

		// see if we already have a value
		if v.hasSnap {
			stream.tracef("startStreamCall: bound to view %p for %#v; already have a snapshot available",
				v, xview)
			stream.ready.Add(i)
			// mark it dirty anyway, in case it needs to
			// be refreshed (this is not something we've
			// really worked out very well...  how to
			// propagate requests from the client to
			// refresh or sync things; we might want an
			// API or annotation that lets the client
			// request a view that bypasses or clears any
			// caching along the way, or some caching
			// (i.e., consider the caching of icon
			// assets... those are long term; we might
			// want something that says "clear everything
			// from the cache that was refreshed less than
			// X time ago" which I think is how Chrome
			// does cache clearing.
			//
			// this approach here is very aggressive; any
			// time a new view is created, we will
			// stimulate a refresh (caching at the target
			// layer may mitigate any actual work, which
			// is why the target layer is encouraged to
			// "trust and and don't even verify" for very
			// recently refreshed items)
			engine.ensureViewDirty(v)
		} else {
			stream.tracef("startStreamCall: bound to view %p for %#v; making sure it is dirty",
				v, xview)
			engine.ensureViewDirty(v)
		}
	}
	engine.dispatchWork()
}

// utility function to ensure a view as dirty, either because a new
// watch came in and we don't have a snapshot already, or because a
// dependency changed
func (engine *Engine[Key, Value, View, Snapshot]) ensureViewDirty(v *view[Key, Value, View, Snapshot]) {
	if v.isDirty {
		return // mission already accomplished
	}
	v.isDirty = true

	t := v.target
	if len(t.dirty) == 0 {
		t.tracef("target needs work because view %p needs eval", v)
		engine.targetWorkQueue.PushBack(t)
	}
	t.dirty = append(t.dirty, v)
}

// Read returns the next snapshot from the stream.  Because Snapshots
// are not comparable or hashable, the engine has no way to check
// whether the snapshots are actually any different than what has been
// returned before.  The contract here is only that the engine returns
// snapshots that *might* have changed.
//
// If that is a consideration for the application, the caller of
// Read() can keep track of which snapshots have been returned for
// which views and filter out redundant ones.
//
// Read is not safe to call concurrently; it should only be called
// once per stream, although Close can be called concurrently with
// Read.
//
// The only errors returned are:
//      - io.EOF if the engine is shut down during the read or
//        if the stream is closed,
//      - context.Canceled if the incoming context is canceled
//
// note that in order to avoid lost updates, it may be that we return
// data even if the context is canceled.  This is a general race condition
func (s *Stream[Key, Value, View, Snapshot]) Read(ctx context.Context) (map[int]Snapshot, error) {
	if s.closed {
		// it was already closed
		return nil, io.EOF
	}
	ret := make(chan error, 1)
	call := &readStreamCall[Key, Value, View, Snapshot]{
		stream: s,
		ret:    ret,
	}
	s.owner.calls <- call

	select {
	case <-ctx.Done():
		s.owner.calls <- cancelReadStreamCall[Key, Value, View, Snapshot]{call}
	case err := <-ret:
		return call.snaps, err
	}

	// we have to close the loop here because the cancel may be
	// racing with data, and we don't want to lose any updates

	select {
	case err := <-ret:
		return call.snaps, err
	}

}

type readStreamCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	stream *Stream[Key, Value, View, Snapshot]
	snaps  map[int]Snapshot
	ret    chan<- error
}

type cancelReadStreamCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	call *readStreamCall[Key, Value, View, Snapshot]
}

func (call cancelReadStreamCall[Key, Value, View, Snapshot]) apply(engine *Engine[Key, Value, View, Snapshot]) {
	// this is just canceling this call to read the stream, which
	// just means removing this call as the waiter on the stream
	stream := call.call.stream

	if stream.waiter != call.call {
		// there could be a race between the read stream
		// closing, the caller getting that information, and the caller
		// canceling.  e.g., consider:
		//    1. we return a value (call.call.ret <- value) and set waiter=nil
		//    2. the caller's context is canceled (they send a cancelReadStreamCall)
		//    3. the caller then gets the result from step 1
		//    4. finally, we get the cancel
		if stream.waiter == nil {
			stream.tracef("(*Stream).Read was already closed but we're processing a cancel")
			return
		}
		stream.tracef("cancel from %p but waiter is %p", call.call, stream.waiter)
		panic("cancel but not the waiter??")
	}
	stream.tracef("canceling (*Stream).Read")
	// but we do send back a context.Canceled error so that the actual
	// stalled call can return
	call.call.ret <- context.Canceled
	stream.waiter = nil
}

type closeStreamCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	stream *Stream[Key, Value, View, Snapshot]
}

func (call closeStreamCall[Key, Value, View, Snapshot]) apply(engine *Engine[Key, Value, View, Snapshot]) {
	// this is closing the stream, which means unlinking it from all its dependencies
	// etc. (reversing the work of Watch, basically)
	stream := call.stream
	stream.tracef("closing")

	if stream.closed {
		// ignore duplicate closes
		return
	}
	stream.closed = true

	// if there is a waiter, it gets an EOF
	if stream.waiter != nil {
		stream.waiter.ret <- io.EOF
		stream.waiter = nil
	}

	// unlink us from all the views
	for _, v := range stream.watches {
		// a stream.watches[k] will be nil if it is
		// a redundant (duplicate) view in the stream
		if v != nil {
			delete(v.streams, stream)
			if len(v.streams) == 0 {
				v.tracef("no more streams watching this view")
				v.shutdown(engine)
			}
		}
	}
}

// Close closes the stream; subsequent attempts to read from the stream will fail, and
// if a Read operation is underway, it will return io.EOF
func (s *Stream[Key, Value, View, Snapshot]) Close() {
	if s.closed {
		// short circuit
		return
	}

	s.owner.calls <- closeStreamCall[Key, Value, View, Snapshot]{s}
}
