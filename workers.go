package viewmaster

import (
	"context"
)

type worker[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	id     int
	engine *Engine[Key, Value, View, Snapshot]
}

type finishWorkCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	a *assignWorkCall[Key, Value, View, Snapshot]
}

type assignWorkCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	// the worker
	worker *worker[Key, Value, View, Snapshot]
	// synchronization on work to do
	ret chan<- error
	// the request (set during assignment)
	target  *target[Key, Value, View, Snapshot]
	views   []*view[Key, Value, View, Snapshot]
	xtarget Target[Key, Value, View, Snapshot]
	xviews  []View
	// the response (returned via finishedWork{})
	snaps []Snapshot
	deps  []map[Key]Value
}

func (call *assignWorkCall[Key, Value, View, Snapshot]) apply(e *Engine[Key, Value, View, Snapshot]) {
	if e.targetWorkQueue.IsEmpty() {
		// alas, nothing to do... queue us up
		call.worker.tracef("assignWorkCall: nothing to do, queuing")
		e.waitingWorkers.PushBack(call)
		return
	}
	// something is already available, resolve immediately
	call.resolve(e.targetWorkQueue.PopFront())
}

// flush anything in the work queue to waiting workers if there are any
func (e *Engine[Key, Value, View, Snapshot]) dispatchWork() {
	// see if there is work to farm out
	for !e.targetWorkQueue.IsEmpty() && !e.waitingWorkers.IsEmpty() {
		w := e.waitingWorkers.PopFront()
		w.resolve(e.targetWorkQueue.PopFront())
	}
}

func (call *assignWorkCall[Key, Value, View, Snapshot]) resolve(t *target[Key, Value, View, Snapshot]) {
	t.tracef("dispatching work: %d views on target %p", len(t.dirty), t)

	call.target = t
	call.xtarget = t.xtarget
	call.xviews = make([]View, len(t.dirty))
	call.views = make([]*view[Key, Value, View, Snapshot], len(t.dirty))

	// mark the view dirty as we assign the work.  NOTE this
	// implies that "isDirty" can and should be true if a view
	// evaluation is in flight and another change comes in that
	// marks it dirty
	//
	// TODO is this appropriate behavior in the case where this is
	// the view's first load, i.e., it doesn't currently have a
	// snap?  Or would it cause us to evaluate a view multiple
	// times if many requests come in?  (though the hammering is
	// limited by the latency of making the call)
	for i, v := range t.dirty {
		call.views[i] = v
		call.xviews[i] = v.xview
		v.isDirty = false // no longer queued
	}
	t.dirty = t.dirty[:0] // zero out the list of dirty views for this target
	call.ret <- nil
}

func (e *Engine[Key, Value, View, Snapshot]) assignWork(w *worker[Key, Value, View, Snapshot]) (*assignWorkCall[Key, Value, View, Snapshot], error) {
	ret := make(chan error, 1)
	a := &assignWorkCall[Key, Value, View, Snapshot]{
		worker: w,
		ret:    ret,
	}
	e.calls <- a
	return a, <-ret
}

func (e *Engine[Key, Value, View, Snapshot]) finishWork(a *assignWorkCall[Key, Value, View, Snapshot]) {
	e.calls <- finishWorkCall[Key, Value, View, Snapshot]{a}
}

func (call finishWorkCall[Key, Value, View, Snapshot]) apply(engine *Engine[Key, Value, View, Snapshot]) {
	a := call.a

	t := a.target
	t.tracef("finishWorkCall: incorporating %d view results from evaluation", len(a.views))

	fup := make(map[*obs[Key, Value, View, Snapshot]]struct{})
	release := make(map[*Stream[Key, Value, View, Snapshot]]struct{})

	for i, v := range a.views {
		t.tracef("finishWorkCall: incorporating [%d] which is view %#v", i, a.xviews[i])

		// store this result to the view
		v.hasSnap = true
		v.snap = a.snaps[i]

		// mark all the watching streams as having ready data
		for s := range v.streams {
			s.ready.Add(s.watchix[v])
			release[s] = struct{}{}
		}

		// unlink the old dependencies
		t.tracef("finishWorkCall: unlinking %d dependencies", len(v.deps))
		for _, d := range v.deps {
			delete(d.views, v)
			if len(d.views) == 0 {
				fup[d] = struct{}{}
			}
		}
		v.deps = v.deps[:0]

		// link in the new dependenceies
		t.tracef("finishWorkCall: linking %d dependencies", len(a.deps[i]))
		for xkey, xval := range a.deps[i] {
			d, isnew := engine.bindObs(xkey, xval)
			if isnew {
				fup[d] = struct{}{}
			}

			d.views[v] = struct{}{}
			v.deps = append(v.deps, d)
		}
	}

	if len(release) > 0 {
		t.tracef("finishWorkCall: releasing %d streams", len(release))
		for s := range release {
			if s.waiter != nil {
				s.waiter.resolve(engine)
				s.waiter = nil
			}
		}
	}

	if len(fup) > 0 {
		t.tracef("finishWorkCall: double-checking %d observables", len(fup))
		var follow, unfollow []*obs[Key, Value, View, Snapshot]

		for d := range fup {
			if len(d.views) == 0 && d.xfollow {
				unfollow = append(unfollow, d)
			} else if len(d.views) > 0 && !d.xfollow {
				follow = append(follow, d)
				d.xfollow = true
			}
		}
		if len(follow) > 0 || len(unfollow) > 0 {
			engine.refollow(follow, unfollow)
		}
	}
}

// run as a worker
func (w *worker[Key, Value, View, Snapshot]) run() {
	defer w.engine.workerWait.Done()

	for {
		assigned, err := w.engine.assignWork(w)
		if err != nil {
			w.tracef("closed: %s", err)
			return
		}
		w.tracef("starting an evaluation")
		snaps, deps, err := assigned.xtarget.Eval(context.TODO(), assigned.xviews)
		if err != nil {
			panic("TODO what to do about this error: " + err.Error())
		}
		w.tracef("done with evaluation")
		assigned.snaps = snaps
		assigned.deps = deps
		w.engine.finishWork(assigned)
	}
}
