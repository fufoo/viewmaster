package viewmaster

type obs[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	key     Key
	state   Value
	views   map[*view[Key, Value, View, Snapshot]]struct{}
	xfollow bool
}

func (engine *Engine[Key, Value, View, Snapshot]) bindObs(k Key, s Value) (*obs[Key, Value, View, Snapshot], bool) {
	i, ok := engine.observables[k]
	if ok {
		return i, false
	}

	i = &obs[Key, Value, View, Snapshot]{
		key:   k,
		state: s,
		views: make(map[*view[Key, Value, View, Snapshot]]struct{}),
	}
	engine.observables[k] = i
	return i, true
}
