package viewmaster

func (e *Engine[Key, Value, View, Snapshot]) run(calls <-chan syscall[Key, Value, View, Snapshot]) {
	for call := range calls {
		call.apply(e)
	}
}
