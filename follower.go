package viewmaster

import (
	"context"
)

func (e *Engine[Key, Value, View, Snapshot]) refollow(follow, unfollow []*obs[Key, Value, View, Snapshot]) {
	e.followChange <- [2][]*obs[Key, Value, View, Snapshot]{unfollow, follow}
}

func (e *Engine[Key, Value, View, Snapshot]) keepFollowing(fups <-chan [2][]*obs[Key, Value, View, Snapshot], f Follower[Key, Value]) {
	defer e.followerWait.Done()

	// note that this is essentially a mirror of the engine's
	// observables index, except that this goroutine owns it, so
	// we don't have to lock it and we can use it to internalize
	// the keys coming from the Follower
	following := make(map[Key]*obs[Key, Value, View, Snapshot])

	apply := func(edits [2][]*obs[Key, Value, View, Snapshot]) {
		for _, d := range edits[0] {
			e.tracef("no longer following %#v", d.key)
			delete(following, d.key)
			f.Unfollow(d.key)
		}
		for _, d := range edits[1] {
			e.tracef("start following %#v", d.key)
			following[d.key] = d
			f.Follow(d.key)
		}
	}

	closing := false

	for {
		e.tracef("following %d dependencies", len(following))
		if len(following) > 0 {
			var edits [2][]*obs[Key, Value, View, Snapshot]
			stream := make(chan map[*obs[Key, Value, View, Snapshot]]Value, 1)
			ctx, cancel := context.WithCancel(context.Background())

			// keep an eye on the fups channel and close
			// this loop if we get a follower update
			go func() {
				defer cancel()

				ed, ok := <-fups
				if !ok {
					e.tracef("keepFollowing: fups closed")
					closing = true
				} else {
					e.tracef("keepFollowing: got -%d+%d edits", len(ed[0]), len(ed[1]))
					edits = ed
				}
			}()

			// call the follower
			go func() {
				defer close(stream)
				for {
					batch, err := f.Read(ctx)
					if err != nil {
						e.tracef("keepFollowing: batch errored: %s", err)
						break
					}

					// translate to internal pointers
					up := make(map[*obs[Key, Value, View, Snapshot]]Value, len(batch))
					for k, v := range batch {
						if d, ok := following[k]; ok {
							e.tracef("keepFollowing: batch includes %#v", k)
							up[d] = v
						} else {
							e.tracef("keepFollowing: batch includes %#v but is not known", k)
						}
					}
					if len(up) > 0 {
						stream <- up
					}
				}
			}()

			for up := range stream {
				e.calls <- dependenciesChangedCall[Key, Value, View, Snapshot]{up}
			}
			if closing {
				e.tracef("keepFollowing: cycle is finished, and closing now")
				return
			}
			apply(edits)
			e.tracef("keepFollowing: cycle is finished, starting again")
		} else {
			e.tracef("keepFollowing: not following anything")
			edits, ok := <-fups
			if !ok {
				e.tracef("keepFollowing: closing now")
				return
			}
			apply(edits)
		}
	}
}

type dependenciesChangedCall[Key comparable, Value Sequenced, View comparable, Snapshot any] struct {
	updates map[*obs[Key, Value, View, Snapshot]]Value
}

func (call dependenciesChangedCall[Key, Value, View, Snapshot]) apply(engine *Engine[Key, Value, View, Snapshot]) {
	for d, v := range call.updates {
		if v.Sequence() > d.state.Sequence() {
			engine.tracef("obs %#v bumping sequence from %d -> %d (dirtying %d views)",
				d.key, d.state.Sequence(), v.Sequence(), len(d.views))
			d.state = v
			for view := range d.views {
				engine.ensureViewDirty(view)
			}
		} else {
			engine.tracef("obs %#v ignored, since sequence %d >= %d",
				d.key, d.state.Sequence(), v.Sequence())
		}
	}
	// dispatch any work, if there is any
	engine.dispatchWork()
}
